<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><? echo $titulo ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Sloy">

        <!-- Le styles -->
        <link href="./css/bootstrap.css" rel="stylesheet">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <link href="./css/general.css" rel="stylesheet">
        <link href="./css/index.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="./img/ico.gif">

        <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCBMUoWZ5_USyMdm8zlOKFhDRyKK-oeOng&sensor=false&language=es&region=ES"></script>
        <script type="text/javascript" src="./js/markerclusterer_packed.js"></script>
        <script type="text/javascript" src="./js/bootstrap.js"></script>
        <script type="text/javascript" src="./js/general.js"></script>
        <script type="text/javascript" src="./js/map.js"></script>
        <script type="text/javascript" src="./js/jquery.ellipsis.js"></script>
        <script type="text/javascript" src="./js/page_index.js"></script>
    </head>

    <body>
        <div class="container-narrow">

            <div class="jumbotron">
                <div id="welcome">
                    <h1>¡Ooops!</h1>
                    <p class="lead">Lo siento, parece que la página a la que intentas acceder no existe. Ten en cuenta que la web está aún en construcción y puede que falten cosas.</p>
                    <a class="btn btn-primary btn-large" href="index.php">Ir al inicio</a>
                </div>
            </div>
        </div> <!-- /container -->
    </body>
</html>