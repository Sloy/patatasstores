<?php
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

$repo = new Repositorio("./php");
$loginHelper = new LoginHelper($repo);
$loginHelper->doLogout();


// Defino algunas constantes
$titulo = "Patatas' Stores";
$repo = new Repositorio("./php");
$categoria = "buscar";
$loginHelper = new LoginHelper($repo);

$query = $_POST['query'];
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><? echo $titulo ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Sloy">

        <!-- Le styles -->
        <link href="./css/bootstrap.css" rel="stylesheet">
        <link href="./css/general.css" rel="stylesheet">
        <link href="./css/buscar.css" rel="stylesheet">

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="./img/ico.gif">

        <script src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCBMUoWZ5_USyMdm8zlOKFhDRyKK-oeOng&sensor=false&language=es&region=ES"></script>
        <script type="text/javascript" src="./js/bootstrap.js"></script>
        <script src="./js/general.js"></script>
        <script src="./js/map.js"></script>
        <script src="./js/page_buscar.js"></script>
    </head>

    <body>
        <div class="container-narrow">
            <? include './parts/header.php'; ?>
        </div>
        <hr>

        <div id="resultados" class="container-fluid">
            <div class="row-fluid">

                <form class="form-search text-right span6 offset5" action="buscar.php" method="post">
                    <div class="input-append ">
                        <input class="search-query" id="query" name="query" placeholder="Nombre, marca, provincia, ciudad..." type="text" <? if(isset($query)){ echo 'value="'.$query.'"';} ?> >
                        <button id="search" class="btn"><i class="icon-search"></i></button>
                    </div>
                    <button class="btn btn-link" type="button">Opciones</button>
                </form>

                <div id="map_canvas" class="span8" style="height: 600px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden; -webkit-transform: translateZ(0);"></div>
                <div id="resultados-list" class="span3">

                    <h4 class="muted">Introduce una búsqueda en el cuadro de arriba </h4>
                </div>
            </div>
        </div>

        <hr>
        <div class="container-narrow">
            <? include './parts/footer.php'; ?>
        </div>
        <!-- /container -->
    </body>
</html>