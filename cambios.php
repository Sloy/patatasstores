<?php
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

$repo = new Repositorio("./php");
$loginHelper = new LoginHelper($repo);

// Defino algunas constantes
$titulo = "Patatas' Stores";
$categoria = "cambios";
$repo = new Repositorio("./php");
$user = $loginHelper->getCurrentUser();

if (isset($formulario)) {
    extract($formulario, EXTR_PREFIX_ALL, "form");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><? echo $titulo ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Sloy">

        <!-- Le styles -->
        <link href="./css/bootstrap.css" rel="stylesheet">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <link href="./css/general.css" rel="stylesheet">
        <link href="./css/cambios.css" rel="stylesheet">

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="./img/ico.gif">

        <script src="./js/date.js"></script>
        <script src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript" src="./js/bootstrap.js"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCBMUoWZ5_USyMdm8zlOKFhDRyKK-oeOng&sensor=false&language=es&region=ES"></script>
        <script type="text/javascript" src="./js/map.js"></script>
        <script src="./js/general.js"></script>
        <script src="./js/page_cambios.js"></script>

    </head>

    <body>

        <div class="container-narrow">

            <? include './parts/header.php'; ?>
            <hr>
            <div>
                <h1>Lista de <abbr title="Commits de mi repositorio Git, por si alguien entiende :p">cambios</abbr> en la aplicación</h1>
                <div id="listacambios">
                    <div class="progress progress-striped active">
                        <div class="bar" style="width: 100%;">Cargando...</div>
                    </div>
                </div>
            </div>


            <hr>


            <? include './parts/footer.php'; ?>

        </div> <!-- /container -->
    </body>
</html>