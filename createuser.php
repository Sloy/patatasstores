<?php
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

$repo = new Repositorio("./php");
$loginHelper = new LoginHelper($repo);

$titulo = "Admin - Patatas' Stores";

$user = $loginHelper->getCurrentUser();

$newUser = $_GET['nombre'];
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><? echo $titulo ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Sloy">

        <!-- Le styles -->
        <link href="./css/bootstrap.css" rel="stylesheet">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <link href="./css/general.css" rel="stylesheet">
        <link href="./css/login.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="./img/ico.gif">

        <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript" src="./js/bootstrap.js"></script>
        <script type="text/javascript" src="./js/general.js"></script>
        <script type="text/javascript" src="./js/page_createuser.js"></script>
    </head>

    <body>

        <div class="container">

            <form class="form-signin" method="get" action="createuser.php" >
                <?
                if (is_null($user) || $user->id != 1) {
                    echo '<div class="alert alert-error"><strong>¡Área privada!</strong> ¿Qué haces aquí? ¡Tira pa\'fuera!</div><a class="btn btn-warning btn-large" href="index.php">Volver</a>';
                } else {
                    if (isset($newUser)) {
                        if ($repo->registrarUsuario($newUser)) {
                            $usuarioCreado = $repo->getUsuarioPorNombre($newUser);
                            if (!is_null($usuarioCreado)) {
                                echo '<div class="alert alert-success"><strong>Ole ole!</strong> Usuario <strong>' . $usuarioCreado->nombre . '</strong> registrado. Utiliza el enlace de abajo para activar la cuenta.</div>';
                                echo '<code>http://patatitas.byethost5.com/setpassword.php?key=' . $usuarioCreado->newuserkey . '</code>';
                            } else {
                                echo '<div class="alert alert-error"><strong>Jo, </strong>algo ha fallado. Pregunta a alguien que sepa cómo arreglarlo :(</div>';
                            }
                        } else {
                            echo '<div class="alert alert-error">Pos por algún motivo no se ha podido hacer el registro :/</div>';
                        }
                    } else {
                        ?>
                        <h4 class="form-signin-heading">¿A quién quieres registrar?</h4>
                        <input id="nombre" autocomplete="off" name="nombre" type="text" class="input-block-level" placeholder="Nombre de usuario, case sensitive" required>
                        <button class="btn btn-large btn-success" type="submit">Registra, registra</button>
                        <?
                    }
                }
                ?>
            </form>
        </div> <!-- /container -->
    </body>
</html>