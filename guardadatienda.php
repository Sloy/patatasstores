<?php
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

$repo = new Repositorio("./php");
$loginHelper = new LoginHelper($repo);

// Defino algunas constantes
$titulo = "Patatas' Stores";
$repo = new Repositorio("./php");
$categoria = "nuevatienda";
$id = $_GET['id'];
if (!isset($id)) {
    header("Location: ./index.php");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><? echo $titulo ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Sloy">

        <!-- Le styles -->
        <link href="./css/bootstrap.css" rel="stylesheet">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <link href="./css/general.css" rel="stylesheet">

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="./img/ico.gif">

        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="./js/general.js"></script>
    </head>

    <body>

        <div class="container-narrow">

            <? include './parts/header.php'; ?>
            <hr>

            <div class="jumbotron">
                <h1>Tienda guardada</h1>
                <p class="lead">¡Gracias! La tienda se ha guardado correctamente. Ahora las demás patatas tendrán otro sitio más dónde comprar. Puedes pasarte por la página de la tienda para dejar tu comentario personal, o añadir otra.</p>
                <a class="btn btn-large btn-success" <?php echo 'href="tienda.php?id='.$id.'"' ?> style="margin-bottom:20px">Ver tienda</a>
                <a class="btn btn-large" href="nuevatienda.php" style="margin-bottom:20px">Añadir otra más</a>
            </div>

            <hr>

            <? include './parts/footer.php'; ?>

        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->



<!--        <script src="../assets/js/bootstrap-transition.js"></script>
<script src="../assets/js/bootstrap-alert.js"></script>
<script src="../assets/js/bootstrap-modal.js"></script>
<script src="../assets/js/bootstrap-dropdown.js"></script>
<script src="../assets/js/bootstrap-scrollspy.js"></script>
<script src="../assets/js/bootstrap-tab.js"></script>
<script src="../assets/js/bootstrap-tooltip.js"></script>
<script src="../assets/js/bootstrap-popover.js"></script>
<script src="../assets/js/bootstrap-button.js"></script>
<script src="../assets/js/bootstrap-collapse.js"></script>
<script src="../assets/js/bootstrap-carousel.js"></script>
<script src="../assets/js/bootstrap-typeahead.js"></script>-->

    </body>
</html>