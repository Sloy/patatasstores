<?php
/* Validación */
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

//Obtiene los datos del formulario
extract($_POST);
$erroresGraves = array();

$repo = new Repositorio("./php");
$loginHelper = new LoginHelper($repo);
$user = $loginHelper->getCurrentUser();

if (!isset($user) || is_null($user)) {
    $erroresGraves[] = "No se encontró ninguna patata logueada";
}

$erroresFormulario = array();

// Nombre
$nombre = htmlspecialchars($nombre);
if (empty($nombre)) {
    $erroresFormulario["nombre"] = "El nombre no puede estar vacío.";
}
if (count($nombre) > 50) {
    $erroresFormulario["nombre"] = "El máximo para el nombre son 50 caracteres.";
}

// Descripción
$descripcion = htmlspecialchars($descripcion);
if (empty($descripcion)) {
    $erroresFormulario["descripcion"] = "La descripción no puede estar vacía.";
}
if (count($descripcion) > 500) {
    $erroresFormulario["descripcion"] = "El máximo para la descripción son 500 caracteres.";
}

// Población
$poblacion = htmlspecialchars($poblacion);
if (empty($poblacion)) {
    $erroresFormulario["poblacion"] = "La poblacion no puede estar vacía.";
}
if (count($poblacion) > 50) {
    $erroresFormulario["poblacion"] = "El máximo para la poblacion son 50 caracteres.";
}

// Indicación
$indicacion = htmlspecialchars($indicacion);
if (empty($indicacion)) {
    $erroresFormulario["indicaciones"] = "La indicación no puede estar vacía.";
}
if (count($indicacion) > 100) {
    $erroresFormulario["indicaciones"] = "El máximo para la indicación son 50 caracteres.";
}

// Posición
$latitud;
$longitud;
if (empty($latitud) || empty($longitud)) {
    $erroresFormulario["posicion"] = "La posición (latitud y longitud) no puede estar vacía.";
}

// Provincia
$provincia;
if ($provincia < 1 || $provincia > 52) {
    $erroresFormulario["provincia"] = "Provincia no válida.";
}
if (empty($provincia)) {
    $erroresFormulario["provincia"] = "Tienes que seleccionar una provincia.";
}

/* Almacenamiento */
if (empty($erroresFormulario) && empty($erroresGraves)) {
    $tienda = new Tienda();
    $loc = new Localizacion();

    try {
        $tienda->nombre = $nombre;
        $tienda->descripcion = $descripcion;
        $tienda->patatita_id = $user->id;
        $tienda->save();

        $loc->latitud = $latitud;
        $loc->longitud = $longitud;
        $loc->poblacion = $poblacion;
        $loc->indicacion = $indicacion;
        $loc->provincia_id = $provincia;
        $loc->tienda_id = $tienda->id;
        $loc->save();
    } catch (Exception $e) {
        $tienda->delete();
        $loc->delete();
        $erroresGraves[] = $e->getMessage();
    }
}

/* Redireccionamiento */
if (empty($erroresFormulario) && empty($erroresGraves)) {
    // Perfecto
    header("Location:  ./guardadatienda.php?id=" . $tienda->id);
} else {
    // Guarda los errores y los valores en la sesión, y redirecciona a la página de nueva tienda
    session_start();
    $_SESSION['erroresFormulario'] = $erroresFormulario;
    $_SESSION['erroresGraves'] = $erroresGraves;
    $_SESSION['formulario'] = $_POST;
    header("Location:  ./nuevatienda.php");
}
?>