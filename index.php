<?php

require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

// Defino algunas constantes
$titulo = "Patatas' Stores";
$repo = new Repositorio("./php");
$categoria = "buscar";
$loginHelper = new LoginHelper($repo);

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><? echo $titulo ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Sloy">

        <!-- Le styles -->
        <link href="./css/bootstrap.css" rel="stylesheet">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <link href="./css/general.css" rel="stylesheet">
        <link href="./css/index.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="./img/ico.gif">

        <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCBMUoWZ5_USyMdm8zlOKFhDRyKK-oeOng&sensor=false&language=es&region=ES"></script>
        <script type="text/javascript" src="./js/markerclusterer_packed.js"></script>
        <script type="text/javascript" src="./js/bootstrap.js"></script>
        <script type="text/javascript" src="./js/general.js"></script>
        <script type="text/javascript" src="./js/map.js"></script>
        <script type="text/javascript" src="./js/jquery.ellipsis.js"></script>
        <script type="text/javascript" src="./js/page_index.js"></script>
    </head>

    <body>
        <div class="container-narrow">

            <? include './parts/header.php'; ?>

            <hr>

            <div class="jumbotron">
                <div id="welcome">
                    <h1>¡Hola patatita!</h1>
                    <p class="lead">En esta web puedes ver tiendas sugeridas por otras patatas en tu zona, o apuntar tus favoritas para que otras sepan dónde ir. Espero que te guste y te resulte útil. Idea a raíz de <a target="_blank" href="http://www.patatasfritas.org/t379-donde-comprar"> este hilo.</a></p>
                    <a class="btn btn-large btn-success" id="welcomeOk" href="#" style="margin-bottom:20px">¡Vale!</a>
                </div>
                <form class="form-search text-right" action="buscar.php" method="post">
                    <div class="input-append ">
                        <input class="span3 search-query" id="query" name="query" placeholder="Nombre, marca, provincia, ciudad..." type="text">
                        <button class="btn" type="submit"><i class="icon-search"></i></button>
                    </div>
                    <button class="btn btn-link" type="button">Opciones</button>
                </form>

                <div id="map_canvas" style="width: 100%; height: 500px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden; -webkit-transform: translateZ(0);"></div>
            </div>

            <hr>

            <div class="row-fluid novedades">
                <div class="span6">
                    <h3>Últimas tiendas</h3>
                    <?php
                    $ultimasTiendas = $repo->getTodasTiendas(5);
                    foreach ($ultimasTiendas as $tienda) {
                        $nombre = $tienda->nombre;
                        $loc = $tienda->localizacion;

                        $provincia = $loc->provincia->nombre;
                        $descripcion = $tienda->descripcion;

                        echo '<h4><a href="tienda.php?id='.$tienda->id.'">' . $nombre . '</a> <span class = "label label-info">' . $provincia . '</span></h4>';
                        echo '<p>' . $descripcion . '</p>';
                    }
                    ?>
                </div>

                <div class="span6">
                    <h3>Últimos comentarios</h3>
                    <?php
                    $ultimosComentarios = $repo->getUltimosComentarios(5);
                    foreach ($ultimosComentarios as $comentario) {
                        $nombre = $comentario->patatita->nombre;
                        $valor = $comentario->valor;
                        $texto = $comentario->comentario;

                        $valoracion = "";
                        if ($valor > 0) {
                            $valoracion = '<span class="badge badge-success"><i class="icon-thumbs-up icon-white"></i></span>';
                        } else if ($valor < 0) {
                            $valoracion = '  <span class="badge badge-important"><i class="icon-thumbs-down icon-white"></i></span>';
                        }
                        echo '<h4><a href="tienda.php?id='.$comentario->tienda_id.'#comment'.$comentario->id.'">' . $nombre . $valoracion . ' </h4>';
                        echo '<p>' . $texto . '</p>';
                    }
                    ?>
                </div>
            </div>

            <hr>

            <? include './parts/footer.php'; ?>

        </div> <!-- /container -->
    </body>
</html>