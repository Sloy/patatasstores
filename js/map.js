var map;
function initializeMap() {
    var mapOptions = {
        center: new google.maps.LatLng(39.8928799002948,  -3.5595703125),
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel:false
    };
    map = new google.maps.Map(document.getElementById("map_canvas"),
        mapOptions);
}


var infowindow = new google.maps.InfoWindow({
    content: "contenidopordefecto",
    maxWidth: 300
});

function showInfoWindow(marker, tienda, zoom){
    var maxLenght = 80;
    var descripcionCorta = tienda.descripcion
    //TODO esto es una chapuza barata
    if(descripcionCorta.length > maxLenght){
        descripcionCorta = descripcionCorta.substring(0, maxLenght) + "...";
    }

    infowindow.setContent("<div class=\"info-window\"><h4>"+tienda.nombre+"</h4><p>"+descripcionCorta+"</p><a href=\"tienda.php?id="+tienda.id+"\">Ver tienda</a></div>");
    infowindow.open(map,marker);
    map.panTo(marker.getPosition());
    if(zoom){
        map.setZoom(14);
    }
}