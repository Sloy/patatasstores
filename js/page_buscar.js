$(document ).ready( function() {
    initializeMap();
    
    google.maps.event.addListener(infowindow, 'closeclick', function(){
        $(".resultado-selected").removeClass("resultado-selected");
    });

    $("form").submit(function(){
        buscar();
        return false;
    });

    var presetQuery = $("#query").val();
    if(presetQuery){
        buscar();
    }

});

function buscar(){
    var query = $("#query").val();
    query = query.trim();
    
    if(query.length!=0){
        console.log("Buscar: "+query);
    
        $.ajax({
            url: "./php/ajax/getTiendasParaMapa.php",
            method: "get",
            data: {
                "query": query
            },
            dataType: "json",
            success: function(json){
                mostrarResultados(json, query);
                mostrarResultadosEnMapa(json, query);
            }
        });
    }else{
        alert("Miarma, introduce algo para buscar...");
    }
}

function mostrarResultados(tiendas, query){
    $("#resultados-list").empty();
    if(tiendas.length!=0){    
        $("#resultados-list").append("<h4>Resultados</h4>");
        $.each(tiendas, function(index, tienda){
            $("#resultados-list").append("<address id=\"tienda"+tienda.id+"\"><strong>"+tienda.nombre+"</strong><br><em>"+tienda.localizacion.poblacion+", "+tienda.localizacion.provincia.nombre+"</em><br>"+tienda.descripcion+"</address>"); 
        });
    }else{
        $("#resultados-list").append("<h4 class=\"muted no-results\">No se encontraron resultados para <em>"+query+"</em>.</h4><h5 class=\"muted\">¿Echas de menos alguna tienda?<br><a href=\"nuevatienda.php\" >Intrudúcela aquí</a></h5>");
    }
    // Hay que volver a asignar el listener
    $("#resultados-list address").click(function(event){
        onResultadoPressed(this, event);
    });
}

var markers = [];
function mostrarResultadosEnMapa(tiendas, query){
    // Limpia el mapa
    deleteOverlays();
    if(tiendas.length>0){
        // Crea los markers, los añade al array y los muestra en el mapa
        $.each(tiendas, function(index,tienda){
            var latLng = new google.maps.LatLng(tienda.localizacion.latitud, tienda.localizacion.longitud);
            var marker = new google.maps.Marker({
                position: latLng,
                title:tienda.nombre,
                animation: google.maps.Animation.DROP
            });
            // Establece el clickListener del marcador para que muestre el globo de información
            google.maps.event.addListener(marker, 'click', function() {
                showInfoWindow(marker, tienda, true);
            });
            // Y lo añade al array de markers para el MarkerCluster
            markers.push(marker); 
            marker.setMap(map);
            
        });
    }
}

function onResultadoPressed(item, event){
    var idStr = item.id.replace("tienda", "");
    var id = parseInt(idStr);
    var pos = $("#resultados-list address").index(item);
    $(".resultado-selected").removeClass("resultado-selected");
    $(item).addClass("resultado-selected");
    google.maps.event.trigger(markers[pos],'click');
}

/**
 * From the official docs
 */
function deleteOverlays() {
    if (markers) {
        for (i in markers) {
            markers[i].setMap(null);
        }
        markers.length = 0;
    }
}