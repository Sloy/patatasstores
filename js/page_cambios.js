$(document ).ready( function() {
    $.ajax({
       url:'https://api.bitbucket.org/1.0/repositories/Sloy/patatasstores/changesets',
       dataType: 'jsonp',
       success: function(json){
           var lista = $("#listacambios");
           lista.empty();
           $(json.changesets.reverse()).each(function(index, item){
               var fecha = Date.parse(item.timestamp);
               var dia = fecha.getDate();
               var mes  = fecha.getMonthName(true);
               var diaSemana = fecha.getDayName(true);
               
               var hora = fecha.getHours();
               var minutos = fecha.getMinutes();
               
               var fechaFormatted = diaSemana+", "+dia+" "+mes+" - "+hora+":"+minutos;
               
               lista.append('<div class="cambio"><strong class="muted">'+fechaFormatted+'</strong><pre>'+item.message+'</pre></div>');
           });
       }
    });
});