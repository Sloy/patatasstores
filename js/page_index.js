$(document ).ready( function() {
    $("#welcomeOk").click(function(){
        $("#welcome").animate({
            opacity: 'hide', 
            height: 'hide'
        }, 'slow');
    })
    
    initializeMap();
    
    /* Markers */
    
    // Obtengo la lista de TODAS las tiendas por ajax
    $.ajax({
        url: "./php/ajax/getTiendasParaMapa.php",
        dataType: 'json',
        success: function(json){
            var markers = [];
            var tiendas = json;
            $.each(json, function(index,tienda){
                // Extrae la posición de la tienda y crea el marcador para el mapa
                var latLng = new google.maps.LatLng(tienda.localizacion.latitud, tienda.localizacion.longitud);
                var marker = new google.maps.Marker({
                    'position': latLng,
                    'title': tienda.nombre
                });
                // Establece el clickListener del marcador para que muestre el globo de información
                google.maps.event.addListener(marker, 'click', function() {
                    showInfoWindow(marker, tienda, false);
                });
                // Y lo añade al array de markers para el MarkerCluster
                markers.push(marker); 
            });
            var mcOptions = {
                gridSize: 40, 
                maxZoom: 12, 
                averageCenter:true,
                //minimunClusterSize:x
                title: "Tiendas en esta zona"
                
            };
            var markerCluster = new MarkerClusterer(map, markers, mcOptions);
        }
    });
});