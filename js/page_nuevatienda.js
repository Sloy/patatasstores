var geocoder;
var marker;

$(document).ready( function() {    
    initializeMap();
    geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
    });
    
    $("#poblacion").focusout(function(event){
        codeAddress();
    });
    $("#poblacionBtn").click(function(event){
        codeAddress();
    });
    $("#provincia").change(function(event){
        provinciaSelected();
    });
    provinciaSelected(); // Hacemos que "cargue" la provincia al refrescar la página
                
    $("form").submit(function(){
        //TODO: Validación
        return true; 
    });
    
});

function provinciaSelected(){
    var codigo = $("#provincia").val();
    if(codigo>-1){
        // opción válida
        // quita el placeholder de la población y la activa
        $("#poblacion").attr('placeholder','');
        $("#poblacion").removeAttr('disabled');
        //$("#poblacion").val(''); //si lo activo me elimina el value cuando viene de un error de validación en el servidor :(
        $("#poblacionBtn").removeAttr('disabled');
        // actualiza el mapa
        codeAddress();
        // Carga las poblaciones en el typeahead
        $.ajax({
            url:"./php/ajax/getPoblaciones.php",
            type: "get",
            data: {
                "provincia": codigo
            },
            dataType:"json",
            success: function(json){
                var autocomplete = $('#poblacion').typeahead();
                autocomplete.data('typeahead').source = json; 
            },
            error:function(error){
                console.log(error);
            }
        });
    }else{
        //opción inválida
        //desactívala y váciala
        $("#poblacion").attr('disabled','');
        $("#poblacion").val('');
        $("#poblacionBtn").attr('disabled','');
    }
}
    
function codeAddress() {
    var provincia = $("#provincia option:selected").text();
    var poblacion = document.getElementById('poblacion').value;
    var query = poblacion+", "+provincia;
    console.log("query: "+query);
                
    geocoder.geocode( {
        'region':'ES', 
        'address': query
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var location = results[0].geometry.location;
            console.log("Latitud: "+location.lat());
            console.log("Longitud: "+location.lng());
            //map.setCenter(location);
            //map.setZoom(14);
            map.fitBounds(results[0].geometry.bounds);
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}
    
function placeMarker(latlng){
    //Quito el antiguo si hay
    if(marker!=null){
        marker.setMap(null);
    }
    //Pongo el marcador
    marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: true,
        title: "¿Dónde está la tienda?"
    });
    //Actualizo los valores del formulario
    $("#latitud").val(latlng.lat());
    $("#longitud").val(latlng.lng());
}

