$(document ).ready( function() {
    $("#submit").removeAttr("disabled");
    
    $("form").submit(function(){
        var rawPassword = $("#passwordVisible").val();
        var rawPassword2 = $("#passwordVisible2").val();
        if(rawPassword != rawPassword2){
            alert("Las contraseñas no coinciden ¬¬");
            return false;
        }else{
            try{
                $("#password").val(md5(rawPassword));
            }catch(err){
                console.log(err);
                $("form").prepend("<div class=\"alert alert-error\"><strong>Ooops, </strong> parece que algo no anda bien. No pude cifrar tu contraseña, y he cancelado la petición por seguridad. ¿Quizá tienes algún bloqueador de scripts?")
                return false;
            }
            return true;
        }
    });
});