$(document).ready(function(){
    initializeMap();
    var pos = $("#posicion").val().split(";");
    var lat = parseFloat(pos[0]);
    var lng = parseFloat(pos[1]);
    var latLng = new google.maps.LatLng(lat,lng);
    map.panTo(latLng);
    map.setZoom(16);
    new google.maps.Marker({
        'position': latLng,
        'map': map
    });
});