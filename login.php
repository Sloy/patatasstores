<?php
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

$repo = new Repositorio("./php");
$loginHelper = new LoginHelper($repo);

$username = $_POST["username"];
$password = $_POST["password"];
$rememberMe = $_POST["remember"];
$refer = $_POST["refer"];

$error = $_SESSION['error'];
unset($_SESSION['error']);

if (isset($username) && isset($password)) {
    try {
        $user = $loginHelper->doLogin($username, $password, $rememberMe);
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
    if (!is_null($user)) {
        if (isset($refer) && !empty($refer)) {
            header("Location: " . $refer);
            exit;
        } else {
            header("Location: ./index.php");
            exit;
        }
    }
}

// Defino algunas constantes
$titulo = "Patatas' Stores";
$repo = new Repositorio("./php");
$categoria = "buscar";
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><? echo $titulo ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Sloy">

        <!-- Le styles -->
        <link href="./css/bootstrap.css" rel="stylesheet">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <link href="./css/general.css" rel="stylesheet">
        <link href="./css/login.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="./img/ico.gif">

        <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript" src="./js/bootstrap.js"></script>
        <script type="text/javascript" src="./js/md5.js"></script>
        <script type="text/javascript" src="./js/general.js"></script>
        <script type="text/javascript" src="./js/page_login.js"></script>
    </head>

    <body>

        <div class="container">

            <form class="form-signin" method="post" action="login.php">
                <?
                if ($error) {
                    ?>
                    <div class="alert alert-error">
                        <strong>Beeep! </strong><? echo $error ?>
                    </div>
                <? } ?>
                <h3 class="form-signin-heading">Identifícate, tubérculo</h3>
                <input id="username" name="username" type="text" class="input-block-level" placeholder="Usuario" required>
                <input id="passwordVisible" type="password" class="input-block-level" placeholder="Contraseña" required>
                <input id="password" name="password" type="hidden">
                <input id="refer" name="refer" type="hidden" value="<? echo $_GET['refer'] ?>" >
                <label class="checkbox">
                    <input type="checkbox" value="remember-me">Acuérdate de mí
                </label>
                <button class="btn btn-large btn-primary" type="submit">Entrar</button>
            </form>

        </div> <!-- /container -->
    </body>
</html>
