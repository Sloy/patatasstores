<?php
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();


$repo = new Repositorio("./php");
$loginHelper = new LoginHelper($repo);
$loginHelper->doLogout();

header("Location: index.php");
?>
