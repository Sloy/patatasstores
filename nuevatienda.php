<?php
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

// Defino algunas constantes
$titulo = "Patatas' Stores";
$repo = new Repositorio("./php");
$categoria = "nuevatienda";
$loginHelper = new LoginHelper($repo);

$user = $loginHelper->getCurrentUser();
if(is_null($user)){
    $_SESSION['error'] = "Para eso tienes que iniciar sesión";
    $url = $_SERVER["REQUEST_URI"];
    $refer = "login.php?refer=".$url;
    header("Location: ".$refer);
    exit;
}


// Control de errores del servidor
$erroresFormulario = $_SESSION['erroresFormulario'];
$erroresGraves = $_SESSION['erroresGraves'];
$formulario = $_SESSION['formulario'];

unset($_SESSION['erroresGraves']);
unset($_SESSION['erroresFormulario']);
unset($_SESSION['formulario']);

if (isset($formulario)) {
    extract($formulario, EXTR_PREFIX_ALL, "form");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><? echo $titulo ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Sloy">

        <!-- Le styles -->
        <link href="./css/bootstrap.css" rel="stylesheet">
        <link href="./css/general.css" rel="stylesheet">
        <link href="./css/nuevatienda.css" rel="stylesheet">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="./img/ico.gif">

        <script src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCBMUoWZ5_USyMdm8zlOKFhDRyKK-oeOng&sensor=false&language=es&region=ES"></script>
        <script type="text/javascript" src="./js/bootstrap.js"></script>
        <script type="text/javascript" src="./js/general.js"></script>
        <script type="text/javascript" src="./js/map.js"></script>
        <script type="text/javascript" src="./js/page_nuevatienda.js"></script>
    </head>

    <body>

        <div class="container-narrow">

            <? include './parts/header.php'; ?>

            <hr>

            <div class="jumbotron">
                <h2>Añade una tienda nueva</h2>
                <div id="welcome" style="display:none;">
                    <p class="lead">En esta web puedes ver tiendas sugeridas por otras patatas en tu zona, o apuntar tus favoritas para que otras sepan dónde ir. Espero que te guste y te resulte útil. Idea a raíz de <a target="_blank" href="http://www.patatasfritas.org/t379-donde-comprar"> este hilo.</a></p>
                    <a class="btn btn-large btn-success" id="welcomeOk" href="#" style="margin-bottom:20px">¡Vale!</a>
                </div>

                <br>
                <?php
                if (!empty($erroresGraves)) {
                    foreach ($erroresGraves as $error) {
                        echo '<div class="alert alert-error">';
                        echo '<strong>Error grave:</strong> ' . $error;
                        echo '</div>';
                    }
                }
                if (!empty($erroresFormulario)) {
                    foreach ($erroresFormulario as $error) {
                        echo '<div class="alert alert-error">';
                        echo '<strong>Error:</strong> ' . $error;
                        echo '</div>';
                    }
                }
                echo $formularioNombre;
                ?>


                <form id="formularioTienda" class="form-horizontal" action="./guardartienda.php" method="post">
                    <div class="control-group">
                        <label class="control-label">Nombre:</label>
                        <div class="controls">
                            <input id="nombre" name="nombre" class="span4" placeholder="Nombre de la tienda" type="text" <? if (isset($form_nombre)) echo 'value="' . $form_nombre . '"' ?> >
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Descripción:</label>
                        <div class="controls">
                            <textarea id="descripcion" name="descripcion" class="span4" placeholder="Escribe una breve descripción de la tienda, cosas que quieras destacar, marcas, trato, etc..." rows="3" ><? if (isset($form_descripcion)) echo $form_descripcion  ?></textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Provincia:</label>
                        <div class="controls">
                            <select id="provincia" name="provincia">
                                <option value="-1">-Elige una-</option>
                                <?php
                                $provincias = $repo->getProvincias();
                                foreach ($provincias as $prov) {
                                    $codigo = $prov->id;
                                    $nombre = $prov->nombre;
                                    if (isset($form_provincia) && $form_provincia == $codigo) {
                                        echo '<option value="' . $codigo . '" selected>' . $nombre . '</option>';
                                    } else {
                                        echo '<option value="' . $codigo . '">' . $nombre . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Población:</label>
                        <div class="controls">
                            <div class="input-append">
                                <input id="poblacion" name="poblacion" class="span4" autocomplete="off" placeholder="Selecciona antes una provincia" type="text"  disabled <? if (isset($form_poblacion)) echo 'value="' . $form_poblacion . '"' ?>>
<!--                                <button id="poblacionBtn" class="btn" type="button" disabled><i class="icon-search"></i></button>-->
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Indicación:</label>
                        <div class="controls">
                            <input id="indicacion" name="indicacion" class="span4" autocomplete="off" placeholder="Ej. una dirección, &QUOT;frente a la estación&QUOT;, ..." type="text" <? if (isset($form_indicacion)) echo 'value="' . $form_indicacion . '"' ?>>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Localización:</label>
                        <div class="controls">
                            <input id="latitud" name="latitud" class="span2" placeholder="Latitud" type="text" readonly <? if (isset($form_latitud)) echo 'value="' . $form_latitud . '"' ?>>
                            <input id="longitud" name="longitud" class="span2" placeholder="Longitud" type="text" readonly <? if (isset($form_longitud)) echo 'value="' . $form_longitud . '"' ?>>
                            <span class="help-block">Pulsa un lugar del mapa para marcar la posición de la tienda.</span>
                        </div>
                    </div>                    
                </form>

                <div id="map_canvas" style="margin-bottom: 30px; width: 100%; height: 500px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden; -webkit-transform: translateZ(0);"></div>

                <div class="row">
                    <p>
                        <button type="submit"  form="formularioTienda" class="btn btn-primary btn-large">Guardar tienda</button>
                        <a href="#" class="btn btn-large">Descartar</a>
                    </p>
                </div>
            </div>

            <hr>

            <? include './parts/footer.php'; ?>

        </div> <!-- /container -->
    </body>
</html>