<?php

$user = $loginHelper->getCurrentUser();
?>
<?php include_once("./parts/analyticstracking.php") ?>
<div class="masthead">
    <ul class="nav nav-pills pull-right">
        
        <li <? if ($categoria == "buscar") echo 'class="active"' ?>><a href="index.php">Buscar</a></li>
        <li <? if ($categoria == "nuevatienda") echo 'class="active"' ?>><a href="nuevatienda.php">Nueva tienda</a></li>
        <li <? if ($categoria == "cambios") echo 'class="active"' ?>><a href="cambios.php">Lista de cambios</a></li>
        <?
        if (is_null($user)) {
            ?>
            <li><a href="login.php">Login</a></li>
            <?
        } else {
            ?>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><? echo $user->nombre ?><b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Añadidas por mi</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Cerrar sesión</a></li>
                </ul>
            </li>
        <? } ?>
    </ul>
    <h3 class="muted"><?
        if (is_null($user)) {
            echo "Patatitas' Stores";
        } else {
            echo "Hola, " . $user->nombre;
        }
        ?>
    </h3>
    <div class="alert"><strong>Ojo: </strong>La web está aún en construcción y algunas cosas pueden no funcionar correctamente. Pero la publico ya para que al menos vayais viéndola y probándola. Cuanto más os guste más la mejoro ;P Enjoy!</div>

</div>