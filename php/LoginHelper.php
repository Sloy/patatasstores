<?php

class LoginHelper {

    private $repo;

    function __construct($repo) {
        $this->repo = $repo;
    }

    /* ----- USER ------ */

    /**
     * Devuelve el usuario conectado en el sistema. Lo busca en la sesión,  
     * si no lo encuentra intenta hacer un login mediante cookies.
     * Si ni con esas, devuelve NULL
     * @return Patatita Usuario logueado.
     */
    function getCurrentUser() {
        $res = $this->getUserFromSession();
        
        if (is_null($res)) {
            $res = $this->getUserFromCookies();
        }

        return $res;
    }

    /**
     * Devuelve el usuario almacenado en la sesión, o NULL si no hay.
     * @return Patatita Usuario logueado en la sesión.
     */
    function getUserFromSession() {
        $user = $_SESSION['user'];
        if (isset($user) && !is_null($user)) {
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Devuelve el usuario guardado en las cookies. Si éstas están presentes hace un login.
     * Si no están presentes o el login falla devuelve NULL.
     * @return Patatita Usuario cuyos datos están almacenados en las cookies.
     */
    function getUserFromCookies() {
        $attrs = $this->getCookieAttrs();
        if (!is_null($attrs) && !empty($attrs)) {
            try {
                $username = $attrs['username'];
                $cookiePass = $attrs['cookiepass'];
                return $this->doLoginWithCookies($username, $cookiePass);
            } catch (Exception $e) {
                return NULL; // Login falla
            }
        } else {
            return NULL; // No están presentes
        }
    }

    /* ----- LOGIN ------ */

    /**
     * Hace un login normal mediante usuario y contraseña.
     * Si falla lanza una excepción.
     * @param String $username Nombre de usuario.
     * @param String $pass Contraseña del usuario.
     * @param Boolean $rememberMe Si el usuario ha elegido ser recordado.
     * @return Patatita Usuario logueado.
     * @throws Exception Si falla el login.
     */
    function doLogin($username, $pass, $rememberMe) {
        $user = $this->repo->getUsuario($username, $pass);
        if (is_null($user) || !isset($user)) {
            throw new Exception("Usuario y contraseña no coinciden.");
        } else {
            // OK
            if ($rememberMe) {
                $cookiepass = $user->cookiepass;
                if (is_null($cookiepass) || empty($cookiepass)) {
                    $cookiepass = $this->generateRandomCookiePass();
                    $user->cookiepass = $cookiepass;
                    $user->save();
                }
                $this->setCookieAttrs($user->nombre, $cookiepass);
            }
            // Uso un puñetero proxy para guardar el objeto en la sesión
            $userProxy = $this->storeInSession($user);
            return $userProxy;
        }
    }

    /**
     * Hace login mediante los datos de cookies.
     * Si falla lanza una excepción.
     * @param String $user nombre de usuario
     * @param String $cookiePass contraseña autogenerada para las cookies
     * @return Patatita Usuario logueado
     * @throws Exception Si falla el login.
     */
    function doLoginWithCookies($username, $cookiePass) {
        $repo = new Repositorio("./php");
        $user = Patatita::first(array("conditions" => array("nombre = '$username' AND cookiepass = '$cookiePass'")));
        if (is_null($user) || !isset($user)) {
            throw new Exception("Usuario y contraseña no coinciden.");
        } else {
            // OK
            $cookiepass = $user->cookiepass;
            if (is_null($cookiepass) || empty($cookiepass)) {
                $cookiepass = $this->generateRandomCookiePass();
                $user->cookiepass = $cookiepass;
                $user->save();
            }
            $userProxy = $this->storeInSession($user);
            return $userProxy;
        }
    }

    /* ----- LOGOUT ------ */

    /**
     * Hace logout al usuario. Destruye la sesión y las cookies.
     */
    function doLogout() {
        unset($_SESSION['user']);
        setcookie(self::COOKIE_NAME, "", time() + 3600, "/");
    }

    /* ----- COOKIES ------ */

    /**
     * Devuelve los atributos del usuario guardados en las cookies en forma de array.
     * Estos son: username y cookiepass
     * @return Array Array asociativo con los atributos. NULL si no hay cookie guardada.
     */
    function getCookieAttrs() {
        $cookie = htmlentities($_COOKIE[self::COOKIE_NAME]);
        if (isset($cookie) && !empty($cookie)) {
            $cookie = explode(self::COOKIE_SEPARATOR, $cookie);
            return array("username" => $cookie[0], "cookiepass" => $cookie[1]);
        } else {
            return NULL;
        }
    }

    /**
     * Guarda en las cookies los datos del usuario para logueo automático.
     * @param String $user Nombre de usuario.
     * @param String $cookiePass Contraseña especial para cookies.
     */
    function setCookieAttrs($user, $cookiePass) {
        $cookie = $user . self::COOKIE_SEPARATOR . $cookiePass;
        setcookie(self::COOKIE_NAME, $cookie, time() + 60 * 60 * 24 * 30, "/"); //duracion de 30 dias de la cookie
    }

    /**
     * Crea una cookie pass aleatoria;
     * @return string Cookie pass
     */
    function generateRandomCookiePass() {
        $length = 50;
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($chars);
        $str;
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        return $str;
    }

    const COOKIE_NAME = "potatocookie";
    const COOKIE_SEPARATOR = ":";

    /* ----- SESSION ----- */

    /**
     * Crea un PatatitaProxy y lo almacena en la sesión.
     * @param type $user Patatita original del modelo.
     * @return \PatatitaProxy El objeto PatatitaProxy creado.
     */
    function storeInSession($user) {
        // Uso un puñetero proxy para guardar el objeto en la sesión
        $userProxy = new PatatitaProxy($user);
        // Y guárdalo en sesión
        $_SESSION['user'] = $userProxy;
        return $userProxy;
    }

}

?>
