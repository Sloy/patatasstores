<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/php-activerecord/ActiveRecord.php';

/**
 * Description of Repositorio
 *
 * @author rafa
 */
class Repositorio {

    function __construct($directorioPHP = ".") {
        ActiveRecord\Config::initialize(function($cfg) use ($directorioPHP) {
                    $cfg->set_model_directory($directorioPHP . '/model');
                    $cfg->set_connections(array(
                        'development' => 'mysql://root:root@127.0.0.1/patatitas;charset=utf8',
                        'production' => 'mysql://b5_12383382:noseque@sql213.byethost5.com/b5_12383382_patatitas_produccion;charset=utf8'
                    ));
                    $cfg->set_default_connection('development');
                });
    }

    function getTodasTiendas($limite = 0) {
        $res;
        if ($limite > 0) {
            $res = Tienda::all(array('limit' => $limite, "include" => array("localizacion"), "order" => "created_at desc, id desc"));
        } else {
            $res = Tienda::all();
        }
        return $res;
    }

    function getTiendaById($id) {
        try {
            return Tienda::find($id);
        } catch (Exception $e) {
            return NULL;
        }
    }

    function getUltimosComentarios($limite = 0) {
        $res;
        if ($limite > 0) {
            $res = Comentario::all(array('limit' => $limite, "include" => array("patatita"), "order" => "created_at desc, id desc"));
        } else {
            $res = Comentario::all();
        }
        return $res;
    }
    
    function getComentariosDeTienda($tiendaId){
        return Comentario::all(array("conditions" => array("tienda_id=$tiendaId")));
    }

    function getProvincias() {
        return Provincia::all();
    }

    function getPoblaciones($provincia = 0) {
        $res = array();
        $locs;
        if ($provincia > 0) {
            $locs = Localizacion::all(array('select' => 'poblacion', "group" => "poblacion", 'conditions' => 'provincia_id =' . $provincia));
        } else {
            $locs = Localizacion::all(array('select' => 'poblacion'));
        }
        foreach ($locs as $l) {
            $res[] = $l->poblacion;
        }

        return $res;
    }

    function getUsuario($username, $pass) {
        return $user = Patatita::first(array("conditions" => array("nombre = '$username' AND password = '$pass'")));
    }

    function getUsuarioFromKey($key) {
        $user = Patatita::first(array("conditions" => array("newuserkey = '$key' AND password IS NULL and newuserkey IS NOT NULL")));
        return $user;
    }

    function setUserPassword($userId, $password) {
        $user = Patatita::find($userId);
        $user->password = $password;
        $user->save();
    }

    function registrarUsuario($nombre) {
        $currentUser = $this->getUsuarioPorNombre($nombre);
        if (!is_null($currentUser)) {
            return false;
        } else {
            $user = new Patatita();
            $user->nombre = $nombre;
            $user->newuserkey = $this->generateRandomKey();
            $user->save();
            return true;
        }
    }

    function generateRandomKey() {
        $length = 20;
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($chars);
        $str;
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        return $str;
    }

    function getUsuarioPorNombre($username) {
        return $user = Patatita::first(array("conditions" => array("nombre = '$username'")));
    }

}

?>
