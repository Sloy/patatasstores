<?php
require_once '../Repositorio.php';
$repo = new Repositorio("..");

$provincia = $_GET['provincia'];
$poblaciones;
if(isset($provincia)){
   $poblaciones = $repo->getPoblaciones($provincia);
}else{
    $poblaciones = $repo->getPoblaciones();
}

echo json_encode($poblaciones);
?>
