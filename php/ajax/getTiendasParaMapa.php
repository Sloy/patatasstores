<?php

require_once '../Repositorio.php';
$repo = new Repositorio("..");

$query = $_GET['query'];
if (isset($query)) {
    // Hago una búsqueda en la base de datos con sql crudo, la librería no me permite tanta flexibilidad
    $select = "select t.id, t.nombre, t.descripcion, l.poblacion, l.indicacion, l.latitud, l.longitud, l.provincia_id, p.nombre as provincia_nombre from tiendas t
                join localizaciones l on l.tienda_id = t.id
                join provincias p on l.provincia_id = p.id
            where 
                t.nombre like \"%$query%\"
                or
                t.descripcion like \"%$query%\"
                or
                l.poblacion like \"%$query%\"
                or
                p.nombre like \"%$query%\"
                or
                indicacion like \"%$query%\"";
    $tiendasFormatoSQL = Tienda::find_by_sql($select);

    // Convierto las tiendas obtenidas a arrays JSON en el formato que esperaría obtener de la bbdd
    $tiendasArray = array();
    foreach ($tiendasFormatoSQL as $tiendaSQL) {
        array_push($tiendasArray, array(
            "id" => $tiendaSQL->id,
            "nombre" => $tiendaSQL->nombre,
            "descripcion" => $tiendaSQL->descripcion,
            "localizacion" => array(
                "latitud" => $tiendaSQL->latitud,
                "longitud" => $tiendaSQL->longitud,
                "poblacion" => $tiendaSQL->poblacion,
                "provincia" => array(
                    "id" => $tiendaSQL->provincia_id,
                    "nombre" => $tiendaSQL->provincia_nombre
                ),
            //"indicacion" => $tiendaSQL->indicacion
            ),
        ));
    }

    echo json_encode($tiendasArray);
} else {
    $tiendas = $repo->getTodasTiendas();

    $tiendasArray = array();
// Construyo un array (de arrays) con los datos que me interesan para el json
    foreach ($tiendas as $t) {
        array_push($tiendasArray, array(
            "id" => $t->id,
            "nombre" => $t->nombre,
            "descripcion" => $t->descripcion,
            "localizacion" => array(
                "latitud" => $t->localizacion->latitud,
                "longitud" => $t->localizacion->longitud
            // "poblacion" => $t->localizacion->poblacion,
            // "indicacion" =>$t->localizacion->indicacion,
            )
        ));
    }

    echo json_encode($tiendasArray);
}
?>
