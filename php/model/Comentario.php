<?php

/**
 * Description of Localizacion
 *
 * @author rafa
 */
class Comentario extends ActiveRecord\Model {

    static $table_name = 'comentarios';
    
    static $belongs_to = array(
        array('patatita', "class_name" => "Patatita")
    );
    
    static $validates_presence_of = array(
        array('comentario')
    );

}

?>
