<?php

/**
 * Description of Localizacion
 *
 * @author rafa
 */
class Localizacion extends ActiveRecord\Model {

    static $table_name = 'localizaciones';
    
    static $belongs_to = array(
        array('provincia', 'class_name' => 'Provincia')
    );
    
    static $validates_presence_of = array(
        array('latitud'), array('longitud'), array('poblacion'));

}

?>
