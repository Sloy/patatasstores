<?php

/**
 * Description of Localizacion
 *
 * @author rafa
 */
class Provincia extends ActiveRecord\Model {

    static $table_name = 'provincias';
   
    static $validates_presence_of = array(
        array('nombre')
    );

}

?>
