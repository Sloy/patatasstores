<?php

/**
 * Description of Tienda
 *
 * @author rafa
 */
class Tienda extends ActiveRecord\Model {

    static $table_name = 'tiendas';
    
    static $has_one = array(
        array('localizacion')
    );
    
    static $belongs_to = array(
        array('patatita', 'class_name' => 'Patatita')
    );
    
    static $has_many = array(
        array('comentarios')
    );
    
    
    static $validates_presence_of = array(
        array('nombre'), array('descripcion') );

}

?>
