<?php
/* Validación */
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

//Obtiene los datos del formulario
extract($_POST);
$erroresGraves = array();
$erroresFormulario = array();

$repo = new Repositorio("./php");
$loginHelper = new LoginHelper($repo);
$user = $loginHelper->getCurrentUser();
$tiendaId = $_REQUEST['id'];

if (!isset($user) || is_null($user)) {
    $erroresGraves[] = "No se encontró ninguna patata logueada.";
}
if(!isset($tiendaId) || is_null($tiendaId)){
    $erroresGraves[] = "No se recibió tienda en la que publicar el comentario.";
}

// Comentario
$comentarioGeneral = htmlspecialchars($comentarioGeneral);
if (empty($comentarioGeneral)) {
    $erroresFormulario["comentario"] = "El comentario general no puede estar vacío.";
}
if (count($comentarioGeneral) > 500) {
    $erroresFormulario["comentario"] = "El máximo para el comentario general son 500 caracteres.";
}

// Organización
$organizacion = htmlspecialchars($organizacion);
if (count($organizacion) > 200) {
    $erroresFormulario["organización"] = "El máximo para la organización son 200 caracteres.";
}

// Dependientes
$dependientes = htmlspecialchars($dependientes);
if (count($dependientes) > 200) {
    $erroresFormulario["poblacion"] = "El máximo para \"dependientes\" son 200 caracteres.";
}

// Muestras
$muestras = htmlspecialchars($muestras);
if (count($muestras) > 100) {
    $erroresFormulario["muestras"] = "El máximo para \"muestras\" son 200 caracteres.";
}


/* Almacenamiento */
$comentario;
if (empty($erroresFormulario) && empty($erroresGraves)) {
    $comentario = new Comentario();
    try {
        $comentario->comentario = $comentarioGeneral;
        $comentario->organizacion = $organizacion;
        $comentario->muestras = $muestras;
        $comentario->dependientes = $dependientes;
        $comentario->valor = $valor;
        $comentario->patatita_id = $user->id;
        $comentario->tienda_id = $tiendaId;
        $comentario->save();
    } catch (Exception $e) {
        $erroresGraves[] = $e->getMessage();
    }
}

/* Redireccionamiento */
if (empty($erroresFormulario) && empty($erroresGraves)) {
    // Perfecto
    header("Location:  ./tienda.php?id=" . $tiendaId. "#comment".$comentario->id);
} else {
    // Guarda los errores y los valores en la sesión, y redirecciona a la página de nueva tienda
    session_start();
    $_SESSION['erroresFormulario'] = $erroresFormulario;
    $_SESSION['erroresGraves'] = $erroresGraves;
    $_SESSION['formulario'] = $_POST;
    header("Location:  ./tienda.php?id=".$tiendaId."#nuevo-comentario");
}
?>