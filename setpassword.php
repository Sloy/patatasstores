<?php
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

$repo = new Repositorio("./php");
$loginHelper = new LoginHelper($repo);
$loginHelper->doLogout();

$titulo = "Patatas' Stores";

$key = $_GET['key'];
$password = $_POST['password'];

$userId = $_SESSION['userId'];
unset($_SESSION['userId']);

if (isset($password) && isset($userId)) {
    if (empty($password) || is_null($password)) {
        $error = "Contraseña vacía.";
    } else {
        //Ok
        $repo->setUserPassword($userId, $password);
        header("Location: login.php");
        exit;
    }
}

$user = $repo->getUsuarioFromKey($key);
if (!is_null($user)) {
    $_SESSION['userId'] = $user->id;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><? echo $titulo ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Sloy">

        <!-- Le styles -->
        <link href="./css/bootstrap.css" rel="stylesheet">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <link href="./css/general.css" rel="stylesheet">
        <link href="./css/login.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="./img/ico.gif">

        <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript" src="./js/bootstrap.js"></script>
        <script type="text/javascript" src="./js/md5.js"></script>
        <script type="text/javascript" src="./js/general.js"></script>
        <script type="text/javascript" src="./js/page_setpassword.js"></script>
    </head>

    <body>

        <div class="container">

            <form class="form-signin" method="post" <?php echo 'action="setpassword.php?key=' . $_GET['key'] . '"'; ?> >
                <?
                if (is_null($user)) {
                    echo '<div class="alert alert-error">Enlace inválido</div>';
                } else {

                    if ($error) {
                        echo '<div class="alert alert-error">' . $error . '</div>';
                    }
                    ?>
                    <h4 class="form-signin-heading">Elige tu contraseña, <? echo $user->nombre ?></h4>
                    <input id="passwordVisible" type="password" class="input-block-level" placeholder="Escribe una contraseña">
                    <input id="passwordVisible2" type="password" class="input-block-level" placeholder="Repítela (no sea que...)">
                    <input id="password" name="password" type="hidden">
                    <button id="submit" class="btn btn-large btn-primary" type="submit" disabled>Registrarme</button>
                <? } ?>
            </form>
        </div> <!-- /container -->
    </body>
</html>