<?php
require_once './php/Repositorio.php';
require_once './php/LoginHelper.php';
require_once './php/PatatitaProxy.php';

session_start();

$repo = new Repositorio("./php");
$loginHelper = new LoginHelper($repo);

// Defino algunas constantes
$titulo = "Patatas' Stores";
$repo = new Repositorio("./php");
$categoria = "nuevatienda";
$user = $loginHelper->getCurrentUser();
$id = $_GET['id'];
$tienda = $repo->getTiendaById($id);
if (!isset($id) || !isset($tienda) || is_null($tienda)) {
    header("Location: ./404.php");
    exit;
}

// Control de errores del servidor
$erroresFormulario = $_SESSION['erroresFormulario'];
$erroresGraves = $_SESSION['erroresGraves'];
$formulario = $_SESSION['formulario'];

unset($_SESSION['erroresGraves']);
unset($_SESSION['erroresFormulario']);
unset($_SESSION['formulario']);

if (isset($formulario)) {
    extract($formulario, EXTR_PREFIX_ALL, "form");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><? echo $titulo ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Sloy">

        <!-- Le styles -->
        <link href="./css/bootstrap.css" rel="stylesheet">
        <link href="./css/bootstrap-responsive.css" rel="stylesheet">
        <link href="./css/general.css" rel="stylesheet">
        <link href="./css/tienda.css" rel="stylesheet">

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="./img/ico.gif">

        <script src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript" src="./js/bootstrap.js"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCBMUoWZ5_USyMdm8zlOKFhDRyKK-oeOng&sensor=false&language=es&region=ES"></script>
        <script type="text/javascript" src="./js/map.js"></script>
        <script src="./js/general.js"></script>
        <script src="./js/page_tienda.js"></script>
    </head>

    <body>

        <div class="container-narrow">

            <? include './parts/header.php'; ?>
            <hr>
            <div class="tienda-content">
                <div class="tienda-header">
                    <div>
                        <h1><? echo $tienda->nombre; ?></h1> 
                        <small>Añadida por <a <? echo 'href="patatita.php?id=' . $tienda->patatita->id . '"' ?>><? echo $tienda->patatita->nombre; ?></a></small>
                    </div>
                    <p class="text-right"><em><? echo $tienda->localizacion->poblacion . ', ' . $tienda->localizacion->provincia->nombre; ?></em><br>
                        <em><? echo $tienda->localizacion->indicacion; ?></em>
                    </p>

                </div>
                <div class="tienda-body">
                    <p class="lead"><? echo $tienda->descripcion; ?></p>
                    <div id="map_canvas" style="width: 100%; height: 350px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden; -webkit-transform: translateZ(0);"></div>
                    <input id="posicion" type="hidden" <? echo 'value="' . $tienda->localizacion->latitud . ';' . $tienda->localizacion->longitud . '"' ?>>
                </div>
            </div>


            <hr>

            <div class="tienda-comentarios">
                <h3>Comentarios sobre la tienda</h3>
                <?php
                $comentarios = $repo->getComentariosDeTienda($tienda->id);
                $meses = array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");



                foreach ($comentarios as $comentario) {

                    $fecha = $comentario->created_at;
                    $dia = $fecha->format("d");
                    $mes = $meses[$fecha->format("n")];
                    $anio = $fecha->format("Y");

                    echo '<div class="comentario" id="comment' . $comentario->id . '">';
                    echo '<div class="comentario-header">';
                    echo '<a href="patatita.php?id=' . $comentario->patatita->id . '">' . $comentario->patatita->nombre . '</a> ';
                    echo '<span class="comentario-fecha" >' . $dia . ' de ' . $mes . ' de ' . $anio . '</span> ';
                    $valor = $comentario->valor;
                    if (isset($valor)) {
                        if ($valor > 0) {
                            echo '<span class="badge badge-success"><i class="icon-thumbs-up icon-white"></i></span>';
                        } else if ($valor < 0) {
                            echo '<span class="badge badge-important"><i class="icon-thumbs-down icon-white"></i></span>';
                        }
                    }
                    echo '</div>';
                    echo '<div class="comentario-body">';
                    echo '<p>' . $comentario->comentario . '</p>';
                    echo '<dl>';
                    if ($comentario->organizacion) {
                        echo '<dt>Organización:</dt>';
                        echo '<dd>' . $comentario->organizacion . '</dd>';
                    }
                    if ($comentario->dependientes) {
                        echo '<dt>Dependientes:</dt>';
                        echo '<dd>' . $comentario->dependientes . '</dd>';
                    }
                    if ($comentario->muestras) {
                        echo '<dt>Muestras:</dt>';
                        echo '<dd>' . $comentario->muestras . '</dd>';
                    }

                    echo '</dl>';
                    echo '</div>';
                    echo '</div>';
                }
                ?>
                <?php
                if (!is_null($user)) {
                    ?>
                    <div class="well" id="nuevo-comentario">
                        <div class="comentario-header">
                            <span class="comentario-fecha" >Deja tu opinión</span>
                        </div>
                        <div class="comentario-body">
                            <?php
                            if (!empty($erroresGraves)) {
                                foreach ($erroresGraves as $error) {
                                    echo '<div class="alert alert-error">';
                                    echo '<strong>Error grave:</strong> ' . $error;
                                    echo '</div>';
                                }
                            }
                            if (!empty($erroresFormulario)) {
                                foreach ($erroresFormulario as $error) {
                                    echo '<div class="alert alert-error">';
                                    echo '<strong>Error:</strong> ' . $error;
                                    echo '</div>';
                                }
                            }
                            ?>
                            <form class="form-horizontal" <? echo 'action="postcomment.php?id=' . $tienda->id . '"' ?> method="post">
                                <div class="control-group">
                                    <label class="control-label">Valoración</label>
                                    <div class="controls">
    <!--                                        <button type="button" class="btn btn-small"><i class="icon-thumbs-up"></i>Me gusta</button>
                                            <button type="button" class="btn btn-small"><i class="icon-thumbs-down"></i>No me gusta</button>-->
                                        <select name="valor" id="valor">
                                            <option value="0">Nah... (sin valoración)</option>
                                            <option value ="1">Me gusta</option>
                                            <option value="-1">No me gusta</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Organización</label>
                                    <div class="controls">
                                        <input type="text" id="organizacion" name="organizacion" placeholder="" <? if (isset($form_organizacion)) echo 'value="' . $form_organizacion . '"' ?>>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Dependientes</label>
                                    <div class="controls">
                                        <input type="text" id="dependientes" name="dependientes" placeholder="" <? if (isset($form_dependientes)) echo 'value="' . $form_dependientes . '"' ?>>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Muestras</label>
                                    <div class="controls">
                                        <input type="text" id="muestras" name="muestras" placeholder="" <? if (isset($form_muestras)) echo 'value="' . $form_muestras . '"' ?> >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Comentario general *</label>
                                    <div class="controls">
                                        <textarea rows="5" id="comentarioGeneral" name="comentarioGeneral" placeholder="" <? if (isset($form_comentarioGeneral)) echo 'value="' . $form_comentarioGeneral . '"' ?> required></textarea>
                                        <span class="help-block warning">*Obligatorio</span>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="controls">
                                        <button type="submit" class="btn btn-primary">Enviar comentario</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <? }else{ ?>
                <div class="alert alert-warning">
                    Para comentar debes iniciar sesión. <a <? echo 'href="login.php?refer=tienda.php?id='.$tienda->id.'"' ?>>Login</a>
                </div>
                
                <? } ?>
                <!--                <button class="btn btn-large btn-block" type="button">¿Algo que decir? ¿Conoces la tienda? ¡Comenta!</button>-->
            </div>

            <hr>

            <? include './parts/footer.php'; ?>

        </div> <!-- /container -->
    </body>
</html>